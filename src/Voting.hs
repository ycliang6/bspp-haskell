{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}

module Voting where
import Control.Monad
import MHMonadLog
import DistrLog
import System.IO.Unsafe
import Data.List

-- A simple model predicting an election outcome
-- given a poll. 

bernoulliPdf r x = if x then r else (1-r)

every n xs = case drop (n-1) xs of
              (y:ys) -> y : every n ys
              [] -> []

-- The poll data
pollData = [True,True,True,False,True,True,False,False,False,False,True,False,False,False,True,True,False,True,True,True,True,True,False,True,False,True,True,False,False,False,True,True,True,False,False,True,True,True,False,False,True,False,True,False,True,False,True,False,False,True,True,False,True,True,True,True,False,False,False,False,True,False,True,False,True,True,False,False,True,True,True,False,False,False,False,True,True,False,False,True,True,False,False,True,False,True,True,False,False,False,False,True,True,False,True,True,False,False,True,False]

-- This model uses likelihood weighting to incorporate the poll data
model :: [Bool] -> Meas Bool
model poll = do
  vote_share <- sample
  mapM (\actual_vote -> score $ bernoulliPdf vote_share actual_vote)
       poll
  return (vote_share > 0.5)

-- This model is more naive and simulates each poll respondant,
-- a sort of rejection sampling.
model2 :: [Bool] -> Meas Bool
model2 poll = do
  vote_share <- sample
  mapM (\actual_vote ->
              do guess_vote <- bernoulli vote_share
                 if guess_vote==actual_vote then score 1 else score 0)
       poll
  return (vote_share > 0.5)

ratio :: [Bool] -> Double
ratio bs = (fromIntegral $ length $ filter id bs) / (fromIntegral $ length bs)

test = do draws <- lwis (model pollData) 100000
          print $ ratio $ take 100 draws


