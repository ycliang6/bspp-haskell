module Mixture where

import MHMonad
import Distr
import Data.Monoid
import Graphics.Rendering.Chart
import Graphics.Rendering.Chart.Backend.Diagrams
import Graphics.Rendering.Chart.State
import Data.Colour
import Data.Colour.Names
import Data.Default.Class
import Control.Lens
import Statistics.Distribution
import Statistics.Distribution.Normal (normalDistr)
import Statistics.Distribution.Beta (betaDistr)


-- A random normal distribution
randomPdfNormal :: Meas (Double -> Double)
randomPdfNormal =
  do a <- normal 3 3
     t <- gamma 1 1 
     return $ normalPdf a (1/(sqrt t))

dataset :: [Double]
dataset = [0.2,4.2,4.1,0.3,3.9,0.5,0.8,4.7,5.2,3.7]

-- A Gaussian mixture model with two clusters
-- Given a 1d dataset, return a measure over pairs of
-- a density function and
-- which cluster each point is in. 
mixture1 :: [Double] -> Meas ((Double -> Double),[Bool])
mixture1 dataset =
  do pdf1 <- randomPdfNormal
     pdf2 <- randomPdfNormal
     p <- uniform 0 1
     assignment <- mapM (\x -> bernoulli p) dataset
     mapM (\(b,x) ->
              if b then score $ pdf1 x
                   else score $ pdf2 x)
       (zip assignment dataset)
     return 
       (\x -> p*(pdf1 x) + (1-p)*(pdf2 x),assignment)



-- Simpler (and faster) version where we
-- incorporate the choice of which point is in which cluster
-- into the likelihood function. 
mixture2 :: [Double] -> Meas (Double -> Double)
mixture2 dataset =
  do pdf1 <- randomPdfNormal
     pdf2 <- randomPdfNormal
     p <- sample
     let pdf x = p*(pdf1 x) + (1-p)*(pdf2 x)
     mapM (\x -> 
          (score $ pdf x))
       dataset
     return pdf    







 -- Some graphing routines
maxlist :: [Double] -> Double
maxlist (x:xs) = max x (maxlist xs)

minlist :: [Double] -> Double
minlist (x:xs) = min x (minlist xs)



every n xs = case drop (n-1) xs of
              (y:ys) -> y : every n ys
              [] -> []

-- More graphing routines
-- epsilon: smallest y axis difference to worry about
-- delta: smallest x axis difference to worry about
interesting_points :: (Double -> Double) -> Double -> Double -> Double -> Double -> [Double] -> [Double]
interesting_points f lower upper epsilon delta acc =
  if abs(upper - lower) < delta then acc
  else
    let mid = (upper - lower) / 2 + lower in
    if abs((f(upper) - f(lower)) / 2 + f(lower) - f(mid)) < epsilon 
    then acc
    else interesting_points f lower mid epsilon delta (mid : (interesting_points f mid upper epsilon delta acc))
 
sample_fun f = 
--  [ (x, f x) | x <- [(-0.25),(-0.25+0.1)..6.2]]
  let xs = ((-0.25) : (interesting_points f (-0.25) 6.2 0.0005 0.1 [6.2])) in
  map (\x -> (x,f x)) xs 

plot_funs :: String -> [Double] -> [Double] -> [Double] -> [Double -> Double] -> IO ()
plot_funs filename blackdata reddata bluedata funs =
  let graphs  = map sample_fun funs                 in
  let my_lines  = plot_lines_style . line_color .~ purple `withOpacity` 0.1
                $ plot_lines_values .~ graphs $ def in
  let my_dots = plot_points_style .~ filledCircles 4 (opaque black)
              $ plot_points_values .~ (map (\x -> (x,0)) blackdata)
              $ def in               
  let my_rdots = plot_points_style .~ filledCircles 4 (opaque red)
              $ plot_points_values .~ (map (\x -> (x,0)) reddata)
              $ def in               
  let my_bdots = plot_points_style .~ filledCircles 4 (opaque blue)
              $ plot_points_values .~ (map (\x -> (x,0)) bluedata)
              $ def in               
  let my_layout = layout_plots .~ [toPlot my_lines , toPlot my_dots, toPlot my_rdots, toPlot my_bdots]
                $ layout_x_axis .
                  laxis_generate .~ scaledAxis def (0,6)
                $ layout_y_axis . laxis_generate .~ scaledAxis def (-0.1,0.7)
                $ def in
  let graphic =  toRenderable my_layout in
  do
     putStr ("Generating " ++ filename ++ "...")
     renderableToFile def filename graphic;
     putStrLn (" Done!")
     return ()




test0 =
  do
    plot_funs "mixture0.svg" dataset [] [] [] 

test1 =
  do
    fs' <- mh $ mixture1 dataset
    let faws = take 100 $ every 1000 $ drop 100 $ fs'
    let fs = map (\((f,_),_) -> f) faws
    let (Just (_,best_assignment)) = lookup (maximum (map snd faws)) (map (\(fa,w) -> (w,fa)) faws)
    plot_funs "mixture.svg" [] (map snd $ filter fst (zip best_assignment dataset)) (map snd $ filter (not . fst) (zip best_assignment dataset)) fs

test2 =
  do
    fs' <- mh $ mixture2 dataset
--    let fs = map (\((f,g,p),_)->(\x -> p*(f x)+(1-p)*(g x))) $ take 10 $ every 1000 $ drop 1000000 $ fs'
    let fs = map (\(f,_)->f) $ take 100 $ every 1000 $ drop 100 $ fs'
    plot_funs "mixture2.svg" dataset [] [] fs


demoData :: [Double]
demoData = concat $ zipWith replicate [5, 4, 3, 7] [0..3]

plotHist :: [Double] -> Renderable (LayoutPick Double Int Int)
plotHist dataset = do
  let histPlot =
          histToPlot $
          plot_hist_values .~  dataset $
          plot_hist_bins .~ 100 $
          defaultPlotHist
  let histLayout =
          layout_x_axis .~ (laxis_title .~ "value" $ def) $
          layout_y_axis .~ (laxis_title .~ "frequency" $ def) $
          layout_plots .~ [histPlot] $
          def
  layoutToRenderable histLayout

mixture_example =
 do p <- bernoulli 0.4
    if p then normal 1 3
    else normal 8 1

test3 =
  do xws <- weightedsamples mixture_example
     renderableToFile def "mixture3.svg" $ plotHist $ map fst $ take 100000 xws
     return ()
