module Pi where

import MHMonad
import Distr

-- Approximating pi via a Monte Carlo simulation.
-- NB this isn't using Bayesian statistics or weighting.

-- Sample (x,y) uniformly and return true if they are in the unit circle. 
model :: Meas Bool
model = do
  x <- uniform (-1) 1
  y <- uniform (-1) 1
  return $ x*x + y*y < 1

-- Look at how many points are in the circle over 1000000 runs.
-- The circle has area pi, the whole square has area 4.
-- So if proportion of true's is points in the circle is p,
-- pi = 4p.
-- (weights are not used here)
test =
  do
    wbs <- weightedsamples model
    let n = 1000000
    let nInCircle = length $ filter id $ map fst $ take n wbs
    let p = (fromIntegral $ nInCircle) / (fromIntegral n)
    print $ show $ 4 * p


-- The version from the lectures, using just a quadrant instead of a circle.
model2 :: Meas Bool
model2 = do
  x <- uniform 0 1
  y <- uniform 0 1
  return $ x*x + y*y < 1

test2 =
  do
    wbs <- weightedsamples model2
    let n = 1000000
    let ins = filter id $ map fst $ take n wbs
    print $ show $ 4 * (fromIntegral $ length ins) / (fromIntegral n)
