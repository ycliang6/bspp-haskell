module Smc where

import MHMonadLog (weightedsamples,sample,score,Meas,lwis)
import Data.Monoid
import Graphics.Rendering.Chart
import Graphics.Rendering.Chart.Backend.Diagrams
import Graphics.Rendering.Chart.State
import Data.Colour
import Data.Colour.Names
import Data.Default.Class
import Control.Lens
import Statistics.Distribution
import Statistics.Distribution.Normal (normalDistr)
import Statistics.Distribution.Beta (betaDistr)
import DistrLog
import Control.Monad.Trans.Writer
import System.IO.Unsafe
import Control.Monad.Extra
import Numeric.Log
import Data.Monoid
import Debug.Trace

{-- An 'internal' version of likelihood weighted importance sampling. 
    Rather than return a list of draws, it returns a probabilistic program that can be 
    sampled from. 
    (This implementation is extremely inefficient because the weightedsamples are not memoized.) --} 
ilwis :: Meas a -> Int -> Meas a
ilwis m n = unsafePerformIO $
  do xws <- weightedsamples m
     let xws' = accumulate (take n xws) 0
     let (_,_,wmax) = last xws'
     return $
       do r <- sample
          let (x,_,_)= head $ filter
                       (\(x,_,wa)-> wa >=
                        (Product$Exp$log r) * wmax)
                       xws'
          return $ x
accumulate [] a = []
accumulate ((x,w):xws) a = (x,w,w+a):(accumulate xws (w+a))

{-- Gaussian random walk --}
randomwalk :: Int -> Meas[Double]
randomwalk 0 = do {r <- normal 0 6 ; return [r]}
randomwalk n = do ~(y:ys) <- randomwalk (n-1)
                  y' <- normal y 1
                  return $ y':(y:ys)

{-- Gaussian random walk and then conditioning --}
kalman :: [Double] -> Meas[Double]
kalman ys =
  do zs <- randomwalk (length ys)
     mapM (\(y,z) -> score (normalPdf z 0.5 y))
          (zip ys (tail zs))
     return zs

{-- Reordered --} 
kalman_reord :: [Double] -> Meas[Double]
kalman_reord []     =
  do {r <- normal 0 6 ; return [r]}
kalman_reord (y:ys) =
  do ~(z:zs) <- kalman ys
     z' <- normal z 1 
     score $ normalPdf z 0.5 y
     return $ z':(z:zs)

{-- Sequential Monte Carlo version of it. --} 
kalmanSmc :: Int -> [Double] -> Meas[Double]
kalmanSmc n []     =
  do {r <- normal 0 6 ; return [r]}
kalmanSmc n (y:ys) =
  do ~(z:zs) <- ilwis (kalmanSmc n ys) n
     z' <- normal z 1 
     score $ normalPdf z 0.5 y
     return $ z':z:zs

observations = [1.0,1.1,3.9,3.2,3.7,3.1] 

 -- Some graphing routines


plot_funs :: String -> [(Double,Double)] -> [[(Double,Double)]] -> IO ()
plot_funs filename dataset funs =
  let graphs  = funs                 in
  let my_lines  = plot_lines_style . line_color .~ blue `withOpacity` 0.5 -- 0.01
                $ plot_lines_values .~ graphs $ def in
  let my_dots = plot_points_style .~ filledCircles 4 (opaque black)
              $ plot_points_values .~ dataset
              $ def in               
  let my_layout = layout_plots .~ [toPlot my_lines , toPlot my_dots]
                $ layout_x_axis .
                  laxis_generate .~ scaledAxis def (0.5,6)
                $ layout_y_axis . laxis_generate .~ scaledAxis def (-0,5) --(-10,10) 
                $ def in
  let graphic =  toRenderable my_layout in
  do
     putStr ("Generating " ++ filename ++ "...")
     renderableToFile def filename graphic;
     putStrLn (" Done!")
     return ()

xs :: [Double]
xs = reverse [0..fromIntegral $ length observations]

test1 =
  do
    yss <- lwis (randomwalk (length observations)) 10000
    plot_funs "randomwalk.svg" [] (map (zip xs) (take 20 yss))

test2 =
  do
    yss <- lwis (kalman observations) 10000
    plot_funs "kalman.svg" (zip (tail xs) observations) (map (zip xs) (take 1000 yss))

test3 =
  do
    yss <- lwis (kalmanSmc 10000 observations ) 10000
    plot_funs "kalmanSmc.svg" (zip (tail xs) observations) (map (zip xs) (take 1000 yss))

test4 =
  do
    plot_funs "positions.svg" (zip (tail xs) observations) [] 

